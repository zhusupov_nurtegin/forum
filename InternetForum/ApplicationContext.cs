﻿using InternetForum.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetForum
{
    public class ApplicationContext : IdentityDbContext<User>
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
     
        public DbSet<Comment> Comments{ get; set; }
        public DbSet<Like> Likes { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)

           : base(options)

        { 
        }

        
    }
    }

