﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetForum.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }

        public int PostId { get; set; }
        public User Post { get; set; }

        public List<Like> Likes { get; set; }
        public DateTime? CommentDate { get; set; }
    }
}
