﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InternetForum.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Theme { get; set; }
        public string Description { get; set; }
        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public List<Comment> Comments { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
