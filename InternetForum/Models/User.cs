﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetForum.Models
{
    public class User : IdentityUser
    {
       
        public string Avatar { get; set; }

        public string BioInfo { get; set;}
        public int AnswerCount { get; set; }
        public string Gender { get; set; }
        public List<Post> Posts { get; set; }
    }
}
