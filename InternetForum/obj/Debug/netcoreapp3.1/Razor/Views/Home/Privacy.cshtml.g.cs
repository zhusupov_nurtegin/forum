#pragma checksum "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\Home\Privacy.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6df57d78b3028b5c90a4301282fe69674763dce9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Privacy), @"mvc.1.0.view", @"/Views/Home/Privacy.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\_ViewImports.cshtml"
using InternetForum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\_ViewImports.cshtml"
using InternetForum.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\_ViewImports.cshtml"
using InternetForum.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\_ViewImports.cshtml"
using PagedList.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6df57d78b3028b5c90a4301282fe69674763dce9", @"/Views/Home/Privacy.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7105f6db90a64709d0fbf7070ea609c1e73a0f3d", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Privacy : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\Home\Privacy.cshtml"
  
    ViewData["Title"] = "Privacy Policy";

#line default
#line hidden
#nullable disable
            WriteLiteral("<h1>");
#nullable restore
#line 4 "C:\Users\ASUS_STRIX\Desktop\InternetForum\InternetForum\Views\Home\Privacy.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</h1>

<div class=""container"">
    <section class=""page-two-col-title"">
     
    </section>
    <section class=""body page-two-col vp-70 m-content-20"">
        <p>Last modified on January 31, 2020.</p>
        <p><strong>1. INTRODUCTION. </strong></p>
        <p>Welcome to Donald J. Trump for President&rsquo;s (&ldquo;we&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo;) official website (the &ldquo;website&rdquo; or &ldquo;site&rdquo;). We understand that visitors to our website may have questions about how this website collects and uses information. Donald J. Trump for President, Inc. (&ldquo;<u>DJTFP</u>&rdquo;) is committed to safeguarding the privacy of our supporters&rsquo; information. This Privacy Policy explains our privacy practices, including the information we collect from you when you use our website, apps, and other digital and online services (collectively, the &ldquo;<u>Services</u>&rdquo;) and how we and our affiliated committees may use that information; our rights to share and disclose s");
            WriteLiteral(@"uch information to third parties; how you can review and modify information that you provide to us; and your preferences regarding our use and disclosure of such information. Please review our <a href=""/terms-of-service/"">Terms of Service</a>, which govern your use of the Services. Any capitalized terms that are not defined in this Privacy Policy shall have the meaning ascribed in the <a href=""/terms-of-service/"">Terms of Service</a></p>
        <p>This Privacy Policy only applies to the information collected by DJTFP and any third parties acting with our authorization or information otherwise provided to us by third parties about you, in each instance, in connection with your use of the Services. This Privacy Policy applies regardless of the computer, mobile phone, tablet, or other electronic device (&ldquo;<u>Device</u>&rdquo;) you use to access the Services and whether you are accessing the Services as a registered user or otherwise.</p>
        <p>By using the Services, you agree that your use of the Se");
            WriteLiteral(@"rvices is governed by this Privacy Policy and our Terms of Service. From time to time, we may update this Privacy Policy. We encourage you to periodically check this Privacy Policy for updates. Your continued use of the Services affirms your agreement to any changes we make to this Privacy Policy.</p>
        <p><strong>2. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INFORMATION WE COLLECT ABOUT YOU.</strong></p>
        <p><strong>A. Registration Information.</strong> In order to access and use certain features, content or functions of the Services (e.g., contributions, communication/networking features, promotions, etc.), you may be asked to provide certain information, including your name, username, password, e-mail, date of birth, gender, address, employment information, and other descriptive information.</p>
        <p><strong>B.</strong>&nbsp;<strong>Contributions and </strong><strong>Payment Information</strong>. If you make any contribution (including any purchase) on or through the Services, your pa");
            WriteLiteral(@"yment information (e.g., credit or debit card type, number and expiration date) and related information (e.g., physical address) may be collected by us and/or our third party payment processors.</p>
        <p>To comply with Federal election law and the regulations of the Federal Election Commission, we are required to make our best efforts to collect and report the name, mailing address, occupation, and name of employer of individuals whose contributions aggregate in excess of $200 per election cycle. Generally, contributions are non-refundable. Should you choose to contribute, you will receive an email confirmation of your contribution.</p>
        <p><strong>C.</strong>&nbsp;<strong>Information You Choose to Provide to Us.</strong> We collect any information you provide to us on or through the Services or in any other way. For example, we collect any information you provide when you update your user account and profile (&ldquo;<u>User Account</u>&rdquo;); participate in contests, surveys, and other promo");
            WriteLiteral(@"tions; and sign up to receive newsletters and other communications.</p>
        <p>In addition, the Services may include features that rely on the use of information stored on, or made available through, your mobile Device. We may also ask to collect or use the specific location of your mobile Device (e.g., by accessing your Device&rsquo;s GPS). If you agree to allow access to your location, you agree that we and our service providers may collect such location-based information from your Device. We may also collect other information based on your location and your Device&rsquo;s proximity to &ldquo;beacons&rdquo; and other similar proximity systems, including, for example, the strength of the signal between the beacon and your Device and the duration that your Device is near the beacon. The type of location data we collect depends on a number of factors, including the type of Device you are using (e.g., laptop, smartphone, tablet) and the type of connection enabled on your Device (e.g., wifi, Bluetooth, broa");
            WriteLiteral(@"dband cable). The DJTFP Apps or other features of the Services may rely on this location information in order to provide certain location-based services, such as DJTFP promotional offers, merchandise offers, or event information or other DJTFP-related content that may be of interest to you. We may also use location information to improve the functionality of the DJTFP Apps and our other applications and services. If you do not wish to have this location information collected and used by DJTFP, you may opt-out by disabling the location and Bluetooth features on your Device and/or in the DJTFP Apps. As a general note, you may choose not to provide certain information, but you may not be able to access certain content or take advantage of certain features of the Services.</p>
        <p>Depending on the Device you are using, we may use third-party services to provide mapping services. For example, DJTFP may use Google maps on certain Devices. You can learn more about the information that Google collects through");
            WriteLiteral(@" its mapping services at <a href=""http://www.google.com/privacy.html"">http://www.google.com/privacy.html</a>.</p>
        <p><strong>D. User Submissions.&nbsp; </strong>You may have the ability to use the Services to communicate with other users of the Services and post comments to or otherwise interact with certain aspects of the Services that are available to the public and/or other users. We may collect any information you submit, post, or provide in the course of such activities, and reserve the right to store any information about the people you contact via the Services.</p>
        <p>Any information, communications, or material of any type or nature that you submit to our Services (including, but not limited to any of our pages contained on a social media platform or website such as Facebook or Twitter) by email, posting, messaging, uploading, downloading, or otherwise (collectively, a &ldquo;<u>Submission</u>&rdquo;), is done at your own risk and without any expectation of privacy. We cannot control");
            WriteLiteral(@" the actions of other users of any social media platform or website and we are therefore not responsible for any content or Submissions contained on such sites and platforms. By visiting any of our pages or websites contained on a social media platform or website, you are representing and warranting to us that you have reviewed the applicable privacy policy and terms of use of that platform or website and that you will abide by all such provisions contained therein.</p>
        <p>Additionally, in the event that we offer a message board or any other interactive or social-type feature on a website administered directly by us, please be aware that these areas may allow you to publicly post, and share with other users, certain messages, content, or other information (e.g., stories, pictures, etc.). Although we may take certain precautions to protect those who use these areas of one of our websites, we encourage you to be wary of giving out any personal information in such public forums. The information you post");
            WriteLiteral(@" can be collected and used by people you do not know. We cannot guarantee the privacy and safety of these areas and are therefore not responsible for any information you choose to post. Your use of these features is fully at your own risk.</p>
        <p><strong>E.</strong>&nbsp;<strong>Usage Data and Technical Information.</strong> The Services include features that automatically collect certain information, which may include the unique Device identifier assigned to your Device, Third Party Platform anonymous identifiers, and other&nbsp;information regarding your use of the Services to, among other things, assist us in authenticating your usage and/or Device, keeping track of your preferences, creating a more tailored user experience, and better serving your particular interests and needs. The Services and/or third parties may use ""cookies"" (which are small data files containing information about you that is stored on your hard drive), ""embedded scripts,"" ""pixel tags,"" and other similar tracking technologie");
            WriteLiteral(@"s (collectively, ""<u>Tracking Technologies</u>"") to collect information automatically as you interact with the Services. Such information may include ""clickstream"" data, the domain name of the service providing you with Internet access, your Device type and attributes, Internet protocol (IP) address, network connection type (e.g., WiFi, 3G, LTE), browser type and version, operating system and platform, the average time spent on the Services, pages viewed, content searched for, access times and other relevant statistics, and your interactions with playlists, audiovisual content, other users of the Services, advertising, and products and services which are offered, linked to or made available on or through the Services. Such information may be collected by us and/or our third-party service providers and partners. Certain Tracking Technologies enable us to customize your experience on the Services and provide tailored recommendations, promotions, advertisements and other content relating to your specific interes");
            WriteLiteral(@"ts. Other Tracking Technologies are used to make login to the Services easier (such as by remembering your user ID) and allow us to collect non-specific location information. You can learn more about the use of cookies and other Tracking Technologies on the Services in <u>Section 4.D</u> below.</p>
        <p><strong>F.&nbsp; Third Party Platform Information.</strong> If you access the Services via your Third Party Platform account (e.g., by logging in to the Services using your Third Party Platform account credentials) and/or integrate your DJTFP account with your Third Party Platform account, we may collect certain information from the Third Party Platform, such as your username and other information you provided to the Third Party Platform in connection with your account (e.g., name, country, date of birth, gender and e-mail address). You can learn more about adjusting your settings and preferences with respect to Third Party Platforms in <u>Section 4.A</u> below.</p>
        <p><strong>3. &nbsp;&nbsp;&n");
            WriteLiteral(@"bsp;&nbsp;&nbsp;&nbsp;&nbsp; HOW WE USE INFORMATION THAT WE COLLECT.</strong></p>
        <p><strong>A.&nbsp;</strong>Except as prohibited by the terms and conditions of any applicable Third Party Platform (e.g., Facebook, Twitter, etc.) and subject to <u>Section 4</u>, we may use information we collect for, among other things, the following purposes:</p>
        <ul>
            <li>Sending you DJTFP and affiliated committees&rsquo; marketing, promotional, e-mails, messages and other correspondence and notifications regarding the Services;</li>
            <li>To notify you about new features and offerings of the Services, including, but not limited to, promotions, events, discounts, news about products and services, and/or special offers;</li>
            <li>Delivery of features, content, services, and products available to you through the Services based on your location;</li>
            <li>Allowing affiliates, service providers, contractors, agents, sponsors, and other third parties to assist us i");
            WriteLiteral(@"n providing and managing the Services;</li>
            <li>Contacting you regarding the administration of any features or functions of the Services you have registered to use;</li>
            <li>Sending you information about your relationship or transactions with us;</li>
            <li>Marketing and promoting the Services, including, without limitation, promotions and other initiatives and activities;</li>
            <li>Where you order goods or services, performing credit checking or other authentication;</li>
            <li>For the prevention and detection of fraud or infringement of our or any third party&rsquo;s rights;</li>
            <li>Responding to your questions or other requests;</li>
            <li>Subject to applicable contractual or legal restrictions, in connection with a sale of all or substantially all of the assets of DJTFP;</li>
            <li>Subject to applicable contractual or legal restrictions, in connection with the sale or exchange of Service user information and re");
            WriteLiteral(@"lated data to a broker, political committee, or other non-profit or for-profit entity;</li>
            <li>Tailoring your experience on the Services and/or otherwise customizing what you see when you visit and use the Services;</li>
            <li>Saving your User Account, registration and profile data or other information (so you do not have to re-enter it each time you visit or use the Services);</li>
            <li>Tracking your return visits to and use of the Services;</li>
            <li>For other purposes disclosed at the time you provide us with the information or which are reasonably necessary to provide the Services or other related product and/or service requested;</li>
            <li>For research purposes, for marketing/promotional purposes and/or to provide anonymous reporting for Third Party Platforms, etc.;</li>
        </ul>
        <ul>
            <li>Accumulating and reporting aggregate, statistical information in connection with the Services and user activity;</li>
           ");
            WriteLiteral(@" <li>Determining which features and services users like best to help us operate the Services, enhance and improve our services and the Services and display advertising and marketing information; and</li>
            <li>Saving certain information for your ongoing use of the Services.</li>
        </ul>
        <p style=""margin-left:36.0pt;"">&nbsp;</p>
        <p><strong>4. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OPTING-OUT OF CERTAIN USES OF YOUR INFORMATION.</strong></p>
        <p><strong>A.</strong>&nbsp;<strong>User Account Privacy Settings</strong>. You may be able to adjust certain privacy, permission and User Account settings by responding to request alerts or notifications that may be presented to you on your Device prior to or following the collection of the relevant information or by navigating to the settings, account or privacy section (as the case may be) of your Device or the applicable Third Party Platform and limiting certain Service functionality (e.g., location services, etc.). Pleas");
            WriteLiteral(@"e be aware that if you limit the collection of certain information, you may not be able to use all of the features and functionality of the Services.</p>
        <p><strong>B. Marketing and Promotional Communications</strong>. At times, you may be presented with the option of whether to subscribe to receive, or be automatically entered to receive, certain marketing and promotional communications, including by text message, from us and/or DJTFP&rsquo;s affiliated committees that we think will be of interest to you. Recipients of such communications can unsubscribe by either following the specific instructions included in such communications or you may &ldquo;opt-out&rdquo; of receiving marketing and promotional e-mails from us at any time by or by sending an email to us at <a href=""/cdn-cgi/l/email-protection#7f1b10111e0b1610110c3f1b10111e131b0b0d0a120f511c1012""><span class=""__cf_email__"" data-cfemail=""dfbbb0b1beabb6b0b1ac9fbbb0b1beb3bbabadaab2aff1bcb0b2"">[email&#160;protected]</span></a> detailing your priva");
            WriteLiteral(@"cy request. Recipients of text messages may &ldquo;opt-out&rdquo; of our mobile messaging program by replying &ldquo;STOP&rdquo; (or &ldquo;QUIT&rdquo;, &ldquo;END&rdquo;, &ldquo;CANCEL&rdquo;, &ldquo;UNSUBSCRIBE&rdquo;, or &ldquo;STOP ALL&rdquo;) to 88022, 88044 or any mobile message you receive from Us, or by contacting Us via the means provided above and clearly communicating your intent to unsubscribe from our mobile messaging program. Other privacy requests can be emailed to <a href=""/cdn-cgi/l/email-protection#fe8e8c97889f9d87be9a91909f929a8a8c8b938ed09d9193""><span class=""__cf_email__"" data-cfemail=""a4d4d6cdd2c5c7dde4c0cbcac5c8c0d0d6d1c9d48ac7cbc9"">[email&#160;protected]</span></a>. Please note that &ldquo;opt-out&rdquo; and &ldquo;unsubscribe&rdquo; requests may not take effect immediately and may take a reasonable amount of time to receive, process, and apply, during which time your information shall remain subject to the prior privacy settings. Additionally, you should be aware that any information p");
            WriteLiteral(@"rovided to third parties prior to your election to opt-out or unsubscribe cannot be retrieved or rescinded by us unless required by applicable law, and you cannot retroactively opt-out or unsubscribe with respect to such third parties.&nbsp;</p>
        <p>Please note that in order to keep you informed about the operation of our Services and related services, we may always send you emails and announcements that are needed for the proper functioning and administration of our Services, including for the purposes described in <u>Section 3</u> above, in connection with your use of such Services. We may also have features that allow you to submit information, questions or feedback to us. In those instances, we may retain your emails and other information you submit to us for our internal purposes, and to help us to serve you better.</p>
        <p><strong>C.</strong>&nbsp;<strong>Privacy</strong> <strong>Preferences</strong>. You may also change certain privacy preferences (e.g., choose to cease receiving certai");
            WriteLiteral(@"n communications or ask us not to share your personal information with a particular third party) by sending an email to us at&nbsp;<a href=""/cdn-cgi/l/email-protection#5727253e2136342e17333839363b332325223a277934383a""><span class=""__cf_email__"" data-cfemail=""f8888a918e999b81b89c979699949c8c8a8d9588d69b9795"">[email&#160;protected]</span></a>&nbsp;detailing your privacy request. If you ask DJTFP to stop using your personal information, we will honor that request, provided that we may retain a record of such information as necessary to comply with applicable law and we may continue to use your contact information to contact you about your Services-related transactions and to respond to your requests, as well as your IP address to determine the country in which you are accessing the Services. Other privacy preferences (e.g., deactivating your User Account, etc.) may be made by modifying your User Account settings on the Services, your Device and/or the applicable Third Party Platform. You always have the right to");
            WriteLiteral(@" access, review and correct the personal information you have provided and generally you may review, update or delete certain information collected by the Services at any time by accessing your User Account as described herein or by contacting us as described in <u>Section 14</u>.</p>
        <p><strong>D. Opting-Out of Use of Certain Information That May be Collected by Tracking Technologies. </strong></p>
        <ul>
            <li><u>Tracking Technologies</u>: Most browsers are initially set to accept cookies (other than Flash cookies) and allow local storage, but you should be able to change your settings to notify you when a cookie is being set or updated, local storage is being used, and/or to block cookies and/or the use of local storage altogether. Please consult the ""Help"" section of your browser for more information. Please note that by blocking any or all cookies you may not have access to certain features, content and/or other personalization available through the Services.</li>
            ");
            WriteLiteral(@"<li><u>Flash Cookies</u>: Users can manage the use of Flash technologies, with the Flash management tools available at Adobe&rsquo;s website, see http://www.adobe.com/products/flashplayer/security/privacy_policy/faq.html.</li>
            <li><u>Do-Not-Track Signals</u>: Certain browsers transmit ""do-not-track"" signals to the websites with which such browsers communicate; however, this feature, and how it is used and activated, varies from browser to browser. Therefore, it is not clear whether the signals are intentionally transmitted by a user, or whether a user is even aware of this. Despite current efforts, there is still disagreement amongst leading Internet standards organizations, industry groups, technology companies and regulators, concerning what, if anything, websites should do when they receive such signals and no standard has been adopted to date. DJTFP takes privacy and security very seriously. With respect to &ldquo;do not track signals,&rdquo; we currently do not take action in response to the");
            WriteLiteral(@"se signals, but if a standard is established and accepted, we may reassess how to respond to these signals.</li>
        </ul>
        <p style=""margin-left:36.0pt;"">&nbsp;</p>
        <p><strong>5. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SHARING AND DISCLOSURE OF INFORMATION WE COLLECT.</strong></p>
        <p><strong>A. Disclosure to Third Parties</strong>. We reserve the right to use, share, exchange and/or disclose to DJTFP affiliated committee and third parties any of your information for any lawful purpose, including, but not limited to, as described in <u>Section 3</u>. You may choose to opt-out of certain such uses at the time such information is collected or by subsequently changing your preferences, as further described in <u>Section 4</u> above.&nbsp;The above excludes text messaging originator opt-in data and consent; this information will not be shared with any third parties.</p>
        <p><strong>B. Disclosure of Certain Information in User Submissions Intended to Be Disclosed</strong>.");
            WriteLiteral(@" Certain information associated with a Submission may be intended for disclosure, such as your username. We will disclose any such information in connection with the display of, and other services relating to, such Submission, and by submitting a Submission on or in connection with the Services you affirmatively consent to the distribution of your information as described herein.</p>
        <p><strong>C. Service Providers, Owners and Licensees</strong>. We may provide and disclose your information to affiliates, operators, service providers, and other third parties for the purpose of providing, offering, supporting, administering and maintaining the Services, and providing the full range of content, activities, services, features, and functions offered on or in connection with the Services.</p>
        <p><strong>D. Additional Disclosures</strong>. There may be instances when DJTFP may access or disclose information without providing you a choice in the following circumstances: (i) to protect or defend the");
            WriteLiteral(@" legal rights or property of DJTFP, our affiliated committees or their employees, agents and contractors (including enforcement of our agreements), (ii) to protect the safety and security of our Services and users, (iii) protect against fraud or for risk management purposes, (iv) if we believe your actions violate this Privacy Policy or Terms of Service<strong>,</strong> or (v) to comply with law or legal process.</p>
        <p><strong>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DATA COLLECTION FROM CHILDREN</strong>.</p>
        <p>This website complies with the Children&rsquo;s Online Privacy Protection Act of 1998. We do not knowingly contact or collect personal information from children under the age of 13 without appropriate parental notice and consent, and our Services are not intended to solicit information of any kind from children under the age of 13. If we are informed that we have unintentionally received personal information from a child under&nbsp; the age of 13, we will delete that inf");
            WriteLiteral(@"ormation. If you want to notify us of our possible receipt of information by children under the age of 13, please contact us at <a href=""/cdn-cgi/l/email-protection#1262607b6473716b526660677f623c717d7f""><span class=""__cf_email__"" data-cfemail=""a7d7d5ced1c6c4dee7c3c8c9c6cbc3d3d5d2cad789c4c8ca"">[email&#160;protected]</span></a>. Please visit <a href=""https://www.ftc.gov/tips-advice/business-center/privacy-and-security/children%27s-privacy"">https://www.ftc.gov/tips-advice/business-center/privacy-and-security/children%27s-privacy</a> for information from the Federal Trade Commission about protecting children&rsquo;s privacy online.</p>
        <p><strong>7. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SECURITY</strong>.</p>
        <p>While no data transmission over the Internet is 100% secure from intrusion, we maintain reasonable physical, electronic, and procedural safeguards to protect your information. However, it is possible that third parties may unlawfully intercept or access transmissions or private comm");
            WriteLiteral(@"unications despite our efforts to prevent this, and we therefore make no guarantees regarding the security of user information.</p>
        <p><strong>8. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; YOUR CALIFORNIA PRIVACY RIGHTS</strong>.</p>
        <p>If you are a resident of the State of California, we provide you with information on how to exercise your disclosure choice options, such as your right to opt-out (which we may sometimes refer to as &ldquo;unsubscribing&rdquo;) or opt-in for use of your information by third parties for marketing purposes. Therefore, pursuant to the California Civil Code, we are not required to maintain or disclose a list of the third parties that received your information for marketing purposes during the preceding year.</p>
        <p>If you are a California resident and wish to request information about how to exercise your third party disclosure choices, please submit your request at <a href=""/cdn-cgi/l/email-protection"" class=""__cf_email__"" data-cfemail=""e090928996818399");
            WriteLiteral(@"a0848f8e818c849492958d90ce838f8d"">[email&#160;protected]</a> or by postal mail to Donald J. Trump for President, c/o Privacy Administrator, 725 Fifth Avenue, New York, New York, 10022, Attn: Your California Privacy Rights. All requests must be labeled &ldquo;Your California Privacy Rights&rdquo; on the email subject line or envelope or post card. For all requests, please clearly state that the request is related to &ldquo;Your California Privacy Rights&rdquo;, include your name, street address, city, state, zip code and e-mail address (your street address is optional if you wish to receive a response to your request via email) and indicate your preference on how our response to your request should be sent (email or postal mail). We will not accept requests via telephone or by fax. We are not responsible for notices that are not labeled or sent properly, or do not have complete information.</p>
        <p><strong>9. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INTEGRATION OF THIRD PARTY PLATFORMS AND SERVICES</");
            WriteLiteral(@"strong>.</p>
        <p>The Services may be linked to, rely on, and/or be integrated with websites, applications, interfaces, services, and/or platforms operated by third party companies, including service providers and Third Party Platforms. DJTFP is not responsible for the privacy practices of any websites, applications, interfaces, services, and platforms operated by such third parties that are linked to, rely on, and/or are integrated with the Services. Once you leave this Services via such a link, access a third party application, interface, service, or platform, including Third Party Platforms and websites operated by service providers, you should check the applicable privacy policy of such third party to determine, among other things, how they will handle any personally identifiable or other information that they may collect from you.</p>
        <p><strong>10. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PRIVACY POLICY CHANGES</strong>.</p>
        <p>From time to time, we may modify this Privacy Policy to refle");
            WriteLiteral(@"ct industry initiatives, third party requirements or changes in the law, technology, our information collection and use practices, or the features and functionality of the Services, and such modifications shall be effective upon posting. When we change this Privacy Policy in a material way, a notice will be posted on our website along with the updated Privacy Policy. Your continued use of the Services after we post a revised Privacy Policy (and, where appropriate, notify you of this change) signifies your acceptance of the revised Privacy Policy. It is therefore important that you review this Privacy Policy regularly to ensure you are updated as to any changes. We may also seek your affirmative consent to the terms of the updated Privacy Policy. For example, we may ask for your consent before implementing new uses of the personal information that we have already collected from you, if such new use was not addressed by the privacy policy under which such information was collected.</p>
        <p><strong>11. &");
            WriteLiteral(@"nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCURACY</strong>.</p>
        <p>It is your responsibility to give us current, complete, truthful, and accurate information, and to keep such information up to date. <strong>We cannot and will not be responsible for any problems or liability that may arise if you do not give us accurate, truthful, or complete information, or you fail to update such information.</strong> We will reject and delete any entry that we believe in good faith to be false, fraudulent, or inconsistent with this Privacy Policy.</p>
        <p><strong>12.</strong><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>INTERNATIONAL USAGE</strong>.</p>
        <p>The Services are controlled, operated, and administered entirely within the United States. If you visit, access, interact with, and/or otherwise use the Services from a location outside the United States, please be advised that any information you provide in connection with any such activity may be processed in and/or transferred to the United S");
            WriteLiteral(@"tates of America and/or other territories and locations, where privacy protections may not be as comprehensive as those in the territory or location where you interact with or otherwise use the Services. By using the Services, you affirmatively consent to the transfer, use, disclosure, provision, and other administration of your information as described herein.</p>
        <p><strong>13. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CONTACT US</strong>.</p>
        <p>If you have any questions about our privacy practices or any of the terms or conditions of this Privacy Policy, please feel free to contact us at:</p>
        <p>Email: <a href=""/cdn-cgi/l/email-protection#85f5f7ecf3e4e6fcc5e1eaebe4e9e1f1f7f0e8f5abe6eae8""><span class=""__cf_email__"" data-cfemail=""d9a9abb0afb8baa099bdb6b7b8b5bdadabacb4a9f7bab6b4"">[email&#160;protected]</span></a></p>
        <p>OR</p>
        <p>Mail:&nbsp;&nbsp; Donald J. Trump for President, Inc.</p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb");
            WriteLiteral(@"sp; Attn: Privacy Matters<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 725 Fifth Avenue<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; New York, New York 10022
        </p>
        <p>&nbsp;</p>
        <p><strong>14. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SOLE STATEMENT</strong>.</p>
        <p>This document is the sole statement of the Services&rsquo; Privacy Policy and no summary, restatement or other version thereof, or other privacy statement or policy, in any form, including, without limitation, machine-generated, is valid.</p>
    </section>
</div>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
