﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InternetForum;
using InternetForum.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using System.Reflection.Metadata.Ecma335;
using InternetForum.ViewModels;


namespace InternetForum.Controllers
{

  
    [Authorize]
    public class PostsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public PostsController(UserManager<User> userManager,ApplicationContext context, IWebHostEnvironment webHostEnvironment)
        {
            _userManager = userManager;
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Posts
        public async Task<IActionResult> Index(int? page)
        {
      
            User user = await _userManager.GetUserAsync(User);
            IEnumerable<Post> posts = _context.Posts.Include(p => p.Comments).Include(pp => pp.User);
            return View(posts);
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts.Include("Comments")
                .FirstOrDefaultAsync(m => m.Id == id);
            post.Comments = _context.Comments.Include("User").Where(c => c.PostId == post.Id).ToList();
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Posts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Theme,Description")] Post post)
        {
            if (ModelState.IsValid)
            {

                post.CreatedOn = DateTime.UtcNow;
                post.LikeCount = 0;
                post.CommentCount = 0;
                User user = await _userManager.FindByNameAsync(User.Identity.Name);
                post.UserId = user.Id;
                _context.Add(post);
             
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
      
            return View(post);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
        
                return NotFound();
            }
        
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Theme,Description")] Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
       
                var post = await _context.Posts
                .FirstOrDefaultAsync(m => m.Id == id);
            
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.Posts.FindAsync(id);

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
        [HttpPost]

        public async Task<IActionResult> Comment(int postId,  string content, DateTime date, List<Like> like)

        {
           
            User user = await _userManager.GetUserAsync(User);
            ViewBag.CurrentUserId = user.Id;
            Post post = await _context.Posts.FirstOrDefaultAsync(m => m.Id == postId);

            Comment comment = new Comment() { PostId = postId, UserId = user.Id , Content = content, CommentDate= DateTime.UtcNow, Likes = like};
            user.AnswerCount = user.AnswerCount + 1;
            post.CommentCount = post.CommentCount + 1;
            _context.Add(comment);
            _context.Update(post);
            await _context.SaveChangesAsync();
            return Ok(Json("{\"count\":" + post.CommentCount +" }"));
        }
        public async Task<IActionResult> Like(int id)
        {
            
            User user = await _userManager.GetUserAsync(User);
            Like like = new Like() { CommentId = id, UserId = user.Id };
             _context.Add(like);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Unlike(int id)
        {
            var like = await _context.Likes.FindAsync(id);
            _context.Likes.Remove(like);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
