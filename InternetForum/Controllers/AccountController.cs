﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;
using InternetForum.Models;
using InternetForum.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace InternetForum.Controllers
{
    public class AccountController : Controller

    {

        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ApplicationContext _context;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context,  IWebHostEnvironment webHostEnvironment)

        {

            _userManager = userManager;

            _signInManager = signInManager;

            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }

        [HttpGet]

        public IActionResult Register()

        {

            return View();

        }

        [HttpPost]

        public async Task<IActionResult> Register(RegisterViewModel model)

        {

            if (ModelState.IsValid)

            {
                string fileName = null;
                if(model.AvatarImage != null)
                {
                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "Images", "AvatarImages");
                     fileName = $"{Guid.NewGuid().ToString()} {model.AvatarImage.FileName}";
                    string filePath = Path.Combine(folderPath, fileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        model.AvatarImage.CopyTo(fileStream);
                    }
                }
                User user = new User { Email = model.Email, UserName = model.UserName, Avatar = fileName};

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)

                {
                    TempData["success"] = "Вы успешно зарегистровались";
                    await _signInManager.SignInAsync(user, false);

                    return RedirectToAction("Index", "Home");

                    

                }

                else

                {

                    foreach (var error in result.Errors)

                    {
                       
                        ModelState.AddModelError(string.Empty, error.Description);

                    }

                }

            }

            return View(model);

        }

        [HttpGet]

        public IActionResult Login(string returnUrl = null)

        {

            return View(new LoginViewModel { ReturnUrl = returnUrl });

        }



        [HttpPost]

        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Login(LoginViewModel model)

        {

            if (ModelState.IsValid)

            {

                User user = await _userManager.FindByEmailAsync(model.Email);

                Microsoft.AspNetCore.Identity.SignInResult result =

                            await _signInManager.PasswordSignInAsync(user,

                                model.Password, model.RememberMe, false);


                if (result.Succeeded)

                {
                    TempData["success"] = "Вы успешно вошли";
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))

                    {

                        return Redirect(model.ReturnUrl);

                    }

                    else

                    {

                        return RedirectToAction("Index", "Home");

                    }

                }

                else

                {
                    TempData["error"] = "Ошибка";
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");

                }

            }

            return View(model);

        }



        [HttpPost]

        [ValidateAntiForgeryToken]

        public async Task<IActionResult> LogOff()

        {

            await _signInManager.SignOutAsync();

            return View();

        }

      //  [HttpPost]
   
  
      
       

    }
}