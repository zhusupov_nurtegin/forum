﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InternetForum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InternetForum.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext _context;
        public HomeController(ILogger<HomeController> logger , ApplicationContext context, UserManager<User> userManager)
        {
            _logger = logger;
            _userManager = userManager;
            _context = context;
        }

        public async Task <IActionResult> Index()
        {
         
            User user = await _userManager.GetUserAsync(User);
          
            return View(user);
        }

        public IActionResult Privacy()
        {
          
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        public async Task<IActionResult> Search(string search)
        {
       
            List<User> users = await _userManager.Users.Where(p => p.Email.Contains(search) || p.UserName.Contains(search)).ToListAsync();

            return View(users);
        }

        public IActionResult SearchAjax()

        {

            return View();

        }


        public IActionResult SearchAjaxResult(string keyWord)

        {

            List<User> users = _context.Users.Where(u =>

                u.UserName.Contains(keyWord) ||

                u.Email.Contains(keyWord)).ToList();

            


            return PartialView("UserSearchResult", users);

        }
    }
}
