﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InternetForum.Migrations
{
    public partial class CreateColumnTheme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Theme",
                table: "Posts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Theme",
                table: "Posts");
        }
    }
}
