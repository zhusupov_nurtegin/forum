﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InternetForum.Migrations
{
    public partial class ColumnCommentIdWASChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Likes_Comments_CommentId1",
                table: "Likes");

            migrationBuilder.DropIndex(
                name: "IX_Likes_CommentId1",
                table: "Likes");

            migrationBuilder.DropColumn(
                name: "CommentId1",
                table: "Likes");

            migrationBuilder.AlterColumn<int>(
                name: "CommentId",
                table: "Likes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Likes_CommentId",
                table: "Likes",
                column: "CommentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_Comments_CommentId",
                table: "Likes",
                column: "CommentId",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Likes_Comments_CommentId",
                table: "Likes");

            migrationBuilder.DropIndex(
                name: "IX_Likes_CommentId",
                table: "Likes");

            migrationBuilder.AlterColumn<string>(
                name: "CommentId",
                table: "Likes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CommentId1",
                table: "Likes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Likes_CommentId1",
                table: "Likes",
                column: "CommentId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_Comments_CommentId1",
                table: "Likes",
                column: "CommentId1",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
